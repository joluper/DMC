# Docker Media Center (DMC) Project - HowTO

El proyecto consta en implementar una estructura de contenedores que ofrezcan servicios de descarga/almacenamiento de archivos, streaming de audio/video y videollamadas en tiempo real. Una de las principales virtudes del proyecto es implementar una autenticación compartida entre todos los servicios mediante LDAP de forma que con las mismas credenciales se pueda a acceder a los tres servicios sin problema alguno.

* Tecnologías: Nexcloud, Ampache, JITSI, MySQL, LDAP.
* Autor: José Luis Pérez Sánchez.
* Motivo: Proyecto de fin de curso EDT ASIX 2018.
* Fechas: 16 de abril de 2018 - 20 de mayo de 2018.
* IP de pruebas: 192.168.2.59/24.

## Arrancado

Para arrancar la infraestructura de contenedores, simplemente tenemos que tener instalada la última versión de Docker y Docker-Compose y ejecutar el siguiente comando:

	docker-compose up -d

Después de esto ya podemos acceder a las urls para atacar a los contenedores probando con las credenciales de nuestra base de datos de ldap, por ejemplo:

	localhost:8080

**(!)** Si la infraestructura de contenedores ya ha sido arrancada, para evitar perder cualquier configuración realizada posteriormente, esta debería ser levantada con el siguiente comando:

	docker-compose start -d

![](../Imágenes/nex_ldap_login.png)

## Añadidos

Adicionalmente, una vez arrancada nuestra infraestructura, es posible aplicar cualquier configuración que requiramos para los servicios de la plataforma, por ejemplo, haciendo uso del usuario administrador de nextcloud (Admin/nexadmin), es posible aplicar permisos a otros usuarios,, cuotas, cifrado, personalización, monitorización y cualquier otra configuración que se preste. Para ampache con el usuario administrador (Admin/ampadmin) es posible añadir catálogos, seleccionar el tema utilizado, abrir canales de broadcasting, entre otros. 
